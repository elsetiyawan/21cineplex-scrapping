
# coding: utf-8

# In[1]:


import requests
import re
import os
import time

from bs4 import BeautifulSoup
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options  


# In[2]:


print("Preparing .....")
hariini = datetime.today().strftime('%Y%m%d')
loc = os.getcwd()
if os.path.isdir(loc+"/"+hariini) == False:
    os.makedirs(hariini)


# In[3]:


options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument("disable-gpu")
browser = webdriver.Chrome(os.getcwd()+'/chromedriver', options=options)


# In[4]:


print("Accessing m.21cineplex.com ..... ")
url_tujuan = "https://m.21cineplex.com/gui.list_city.php?sid="
url = requests.get(url_tujuan)
content = url.content
rapi = BeautifulSoup(content,"html.parser")


# In[ ]:


print("Grab city and movie data .....")
cities = []
cari = rapi.find_all("li", {"class": "list-group-item"})
for x in cari:
    elem = x.find("div")
    city_name = elem.text
    link = elem['onclick']
    city_id = re.search("city_id=(.*)'",link)
    city = {
        'city_id' : city_id.group(1),
        'city_name' : city_name
    }
    cities.append(city)


# In[ ]:


for x in cities:
    hitlink = browser.get('https://m.21cineplex.com/gui.list_theater.php?sid=&city_id='+x['city_id'])
    hitlink = browser.get('https://m.21cineplex.com/index.php?sid=')
    daftar = browser.find_elements_by_xpath("//div[@class='grid_movie']/a")
    hitung = 1
    movies = []
    for x in daftar:
        judul = browser.find_element_by_xpath("//div["+str(hitung)+"][@class='grid_movie']/div[@class='title']")
        link = browser.find_element_by_xpath("//div["+str(hitung)+"][@class='grid_movie']/a")
        mov_id = re.search("movie_id=(.*)", link.get_attribute('href'))
        judul_baru = judul.text.replace(':','').replace('-','')
        data = {
            'judul' : judul_baru,
            'id_mov' : mov_id.group(1)
        }
        movies.append(data)
        hitung += 1
        
    for x in movies:
        judul = x['judul']
        f = open(loc+"/"+hariini+"/"+judul+".txt",'a+')
        print(judul)
        mov_id = x['id_mov']
        akses = browser.get('https://m.21cineplex.com/gui.schedule.php?sid=&find_by=2&movie_id='+mov_id)
        jadwal = browser.find_elements_by_xpath("//li[@class='list-group-item']")
        count = 1
        for x in jadwal:
            lokasi = browser.find_element_by_xpath("//li[@class='list-group-item']["+str(count)+"]/a")
            tanggal = browser.find_element_by_xpath("//li[@class='list-group-item']["+str(count)+"]/div/div[@class='col-xs-7']")
            tiket = browser.find_element_by_xpath("//li[@class='list-group-item']["+str(count)+"]/div/div[@class='col-xs-5']")
            jam = browser.find_element_by_xpath("//li[@class='list-group-item']["+str(count)+"]/div/p[@class='p_time']")

            f.write(lokasi.text.strip()+"\n")
            f.write(tanggal.text.strip()+tiket.text.strip()+"\n")
            f.write(jam.text.strip())
            f.write("\n")

            print(lokasi.text)
            print(tanggal.text)
            print(tiket.text)
            print(jam.text)
            count += 1
        f.close()


# In[ ]:


exit()

